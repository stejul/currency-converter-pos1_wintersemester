-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Jan 2019 um 13:10
-- Server-Version: 10.1.28-MariaDB
-- PHP-Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `patterntask`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `log` varchar(255) NOT NULL,
  `stamp` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `log`
--

INSERT INTO `log` (`id`, `log`, `stamp`) VALUES
(1, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(2, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-22'),
(3, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-22'),
(4, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-22'),
(5, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(6, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(7, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(8, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(9, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(10, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(11, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(12, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(13, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(14, 'Convert to, EUR2USD with the value of: 5.0', '2019-01-22'),
(15, 'Convert to, EUR2YEN with the value of: 4.0', '2019-01-22'),
(16, 'Convert to, EUR2YEN with the value of: 4.0', '2019-01-22'),
(17, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-22'),
(18, 'Convert to, EUR2YEN with the value of: 4.0', '2019-01-22'),
(19, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-22'),
(20, 'Convert to, EUR2YEN with the value of: 4.0', '2019-01-23'),
(21, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-23'),
(22, 'Convert to, EUR2YEN with the value of: 4.0', '2019-01-23'),
(23, 'Convert to, EUR2USD with the value of: 2.0', '2019-01-23'),
(24, 'Convert to, EUR2YEN with the value of: 4.0', '2019-01-23');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
