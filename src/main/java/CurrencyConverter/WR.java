package CurrencyConverter;

/**
 * Implementing Conversion Interface
 * Every currency converter will implement these methods
 * It follows the Chain of Responsibilty Pattern
 *
 * @author Stefan Mikic
 * @version 1
 * */
public abstract class WR implements IUmrechnen {

    private String currencyFrom;
    private String currencyTo;
    protected double rate;
    private WR successor;


    public WR getSuccessor() {
        return this.successor;
    }

    public void setSuccessor(WR successor) {
        this.successor = successor;
    }


    public String getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyFrom(String currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    /**
     * The method should be implemented using the <b>Template Method </b> Pattern
     * @param currencyVariant Defining which type of currency should be converted
     * @param number Defining which value should be converted
     * @return convertedResult
     * */
    @Override
    public double umrechnen(String currencyVariant, double number) {
        double convertedResult = 0;
        //Implementing Template Method
        if (currencyVariant.equals(this.getCurrencyVariant())) {
            convertedResult = number * this.getRate();
            return convertedResult;
        } else if (successor != null){
            convertedResult = successor.umrechnen(currencyVariant, number);
        }
        return convertedResult;
    }


    /**
     *
     * @param successor Adds a new successor to the responsibility chain
     * */
    public void addNextSuccessor(WR successor) {
        this.successor = successor;
    }

    /**
     * Checks if the object has a successor and removes it from the chain.
     * @return true/false
     */
    public boolean removeLastSuccessor() {
        boolean check = false;
        WR vorherigerSuccessor = this;
        WR momentan = this;
        while (momentan.getSuccessor() != null) {

            vorherigerSuccessor = momentan;
            momentan = momentan.getSuccessor();
            check = true;

        }
        vorherigerSuccessor.setSuccessor(null);

        return check;
    }

    public String getCurrencyTo() {
        return currencyTo;
    }

    public void setCurrencyTo(String currencyTo) {
        this.currencyTo = currencyTo;
    }

    /**
     *
     * Needed for the Template Method Pattern
     */
    public abstract String getCurrencyVariant();

    /**
     *
     * Needed for the Template Method Pattern
     */
    public abstract double getRate();


    public void setRate(double rate) {
        this.rate = rate;
    }
}
