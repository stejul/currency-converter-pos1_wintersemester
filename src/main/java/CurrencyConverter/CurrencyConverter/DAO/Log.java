package CurrencyConverter.CurrencyConverter.DAO;

import java.sql.Date;

/**
 * Log Class for the DAO implementation
 * @author Stefan Mikic
 * @version 1
 */
public class Log {

    private int id;
    private String log;
    private Date stamp;

    /**
     * Creating a Log Constructor for the DAO Implemenation
     * @param log String message of the log
     * @param stamp Current Date when the log was created
     */
    public Log(String log, Date stamp) {
        this.id = id;
        this.log = log;
        this.stamp = stamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Date getStamp() {
        return stamp;
    }

    public void setStamp(Date stamp) {
        this.stamp = stamp;
    }
}
