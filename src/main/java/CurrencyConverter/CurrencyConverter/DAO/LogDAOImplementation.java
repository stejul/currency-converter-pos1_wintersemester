package CurrencyConverter.CurrencyConverter.DAO;

import java.sql.*;

/**
 * Implementing the DAO interface using JDBC mysql connector
 * @author Stefan Mikic
 * @version 1
 * @see LogDAO
 */
public class LogDAOImplementation implements LogDAO {

    /**
     * Using JDBC and prepared statements to insert the logs from the WRDecoratorLogging into the MySQL Database
     * @param log
     */
    @Override
    public void addLog(Log log) {

        try {
            Connection verbindung = Conn.getCon("root", "stefan");
            PreparedStatement pstms = verbindung.prepareStatement("INSERT into log (log, stamp) VALUES (?, ?)");

            pstms.setString(1, log.getLog());
            pstms.setDate(2, log.getStamp());
            pstms.executeUpdate();
            pstms.close();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getSQLState());
        }
    }

    /**
     * Using JDBC prepared statements to delete the logs from the database
     * @param id ID from the table
     */
    @Override
    public void deleteLog(int id) {

        try {
            Connection verbindung = Conn.getCon("root", "stefan");
            PreparedStatement pstms = verbindung.prepareStatement("DELETE from log WHERE id=?");

            pstms.setInt(1, id);
            pstms.executeUpdate();
            pstms.close();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getSQLState());
        }
    }

}

