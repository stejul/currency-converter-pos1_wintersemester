package CurrencyConverter.CurrencyConverter.DAO;

import java.sql.Date;
import java.sql.Timestamp;


/**
 * Interface for the DAO implementation
 * @author Stefan Mikic
 * @version 1
 */
public interface LogDAO {

    /**
     * add logs to database
     * @param log
     */
    public void addLog(Log log);

    /**
     * Delete logs from database
     * @param id ID from the table
     */
    public void deleteLog(int id);
}
