package CurrencyConverter.CurrencyConverter.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Singleton Pattern class for the database connection
 * @author Stefan Mikic
 * @version 1
 */
public class Conn {

    private static Connection con;


    private Conn() {
    }

    /**
     * Checks if a connection is already established and if not, the construction creatas a new connection to the mysql database
     * @param username database username
     * @param password database password
     * @return connection
     */
    public static Connection getCon(String username, String password) {
        if (con == null) {
            try {
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/patterntask?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", username, password);
                return con;

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                System.out.println(ex.getErrorCode());
                System.out.println(ex.getSQLState());
                return null;
            }

        } else {
            return con;
        }
    }
}
