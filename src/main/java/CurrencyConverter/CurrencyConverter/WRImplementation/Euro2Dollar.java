package CurrencyConverter.CurrencyConverter.WRImplementation;

import CurrencyConverter.WR;

/**
 * One of the implementations using the WR abstract class
 * @author Stefan Mikic
 * @version 1
 */
public class Euro2Dollar extends WR {

    private String currencyVariant;

    /**
     * Constructor without setting the successor
     */
    public Euro2Dollar(){
        this.setRate(1.14);
        this.currencyVariant = "EUR2USD";
        this.setCurrencyFrom("EUR");
        this.setCurrencyTo("USD");
    }

    /**
     * Constructor setting with the successor
     * @param successor setting the successor
     */
    public Euro2Dollar(WR successor){
        this.setRate(1.14);
        this.currencyVariant = "EUR2USD";
        this.setCurrencyFrom("EUR");
        this.setCurrencyTo("USD");
        this.setSuccessor(successor);

    }
    /**
     * Implementing the abstract methods for the Template Method Pattern
     * @return rate Conversion rate
     */
    @Override
    public double getRate() {
        return this.rate;
    }

    /**
     * Implementing the abstract method for the Template Method pattern
     * @return currencyVariant
     */
    @Override
    public String getCurrencyVariant(){
        return currencyVariant;
    }
}
