package CurrencyConverter.CurrencyConverter.WRImplementation;

import CurrencyConverter.WR;

/**
 * One of the implementations using the WR abstract class
 * @author Stefan Mikic
 * @version 1
 */
public class Dollar2serbDinar extends WR {

    private String currencyVariant;


    /**
     * Constructor without setting the successor
     */
    public Dollar2serbDinar(){
        this.setRate(123.35);
        this.currencyVariant = "USD2DNR";
        this.setCurrencyFrom("USD");
        this.setCurrencyTo("DNR");
    }

    /**
     * Constructor setting with the successor
     * @param successor setting the successor
     */
    public Dollar2serbDinar(WR successor){
        this.setRate(123.35);
        this.currencyVariant = "USD2DNR";
        this.setCurrencyFrom("USD");
        this.setCurrencyTo("DNR");
        this.setSuccessor(successor);

    }

    /**
     * Implementing the abstract methods for the Template Method Pattern
     * @return rate Conversion rate
     */
    @Override
    public double getRate() {
        return this.rate;
    }

    /**
     * Implementing the abstract method for the Template Method pattern
     * @return currencyVariant
     */
    @Override
    public String getCurrencyVariant(){
        return currencyVariant;
    }
}
