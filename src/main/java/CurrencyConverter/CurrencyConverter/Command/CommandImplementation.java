package CurrencyConverter.CurrencyConverter.Command;

import CurrencyConverter.WR;

/**
 * Implementation of the Command interface
 * @author Stefan Mikic
 * @version 1
 * @see ICommand
 */
public class CommandImplementation implements ICommand {
    private String currencyVariant;
    private double number;
    private double conversionResult;
    private WR cor;

    /**
     * Constructor which initializes the Currency Converter, currency variant and the value for the Command Pattern
     * @param cor WR Object
     * @param currencyVariant which type of currency should be converted to
     * @param number the value of the source currency
     */
    public CommandImplementation(WR cor, String currencyVariant, double number){
        this.currencyVariant = currencyVariant;
        this.number = number;
        this.cor = cor;

    }

    /**
     * Executing the conversion method from the WR class
     * @see WR
     */
    @Override
    public void execute() {
        this.conversionResult= this.cor.umrechnen(this.currencyVariant, this.number);

    }

    /**
     * Returning the conversionValue
     * @return conversionValue
     */
    @Override
    public double getValue() {
        return this.conversionResult;

    }
}
