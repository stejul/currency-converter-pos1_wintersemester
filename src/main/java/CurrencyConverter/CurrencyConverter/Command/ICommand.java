package CurrencyConverter.CurrencyConverter.Command;

/**
 * Interface for the Command Pattern
 * @author Stefan Mikic
 * @version 1
 */
public interface ICommand {

//TODO Undo/Redo + logging
    public void execute();

    public double getValue();
}