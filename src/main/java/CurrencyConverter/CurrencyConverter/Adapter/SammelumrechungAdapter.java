package CurrencyConverter.CurrencyConverter.Adapter;

import CurrencyConverter.ISammelumrechnung;
import CurrencyConverter.WR;

/**
 * Class which implements the bulk conversion
 */
public class SammelumrechungAdapter implements ISammelumrechnung {

    private WR rechner;

    /**
     * Constructor for the bulk conversion
     * @param rechner
     */
    public SammelumrechungAdapter(WR rechner) {
        this.rechner = rechner;
    }


    /**
     * Method for splitting the array and converting the values to the source currency
     * @param currencyVariant which type of currency should be converted
     * @param number array with multiple values
     * @return sum of the converted values
     */
    @Override
    public double umrechnen(String currencyVariant, double[] number) {
        double arrayNumber = 0.;

        for (int i = 0; i < number.length; i++) {
            double nr = number[i];
            arrayNumber += rechner.umrechnen(currencyVariant, nr);
        }
        return arrayNumber;
    }
}
