package CurrencyConverter.CurrencyConverter.Decorator;


import CurrencyConverter.CurrencyConverter.DAO.Log;
import CurrencyConverter.CurrencyConverter.DAO.LogDAO;
import CurrencyConverter.CurrencyConverter.DAO.LogDAOImplementation;
import CurrencyConverter.WR;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * WRDecorator class with logging capabilities + DAO implementation
 * @author Stefan Mikic
 * @version 1
 * @see WRDecorator
 */
public class WRDecoratorLogging extends WRDecorator {

    public WRDecoratorLogging(WR wrDecorator) {
        super(wrDecorator);
    }


    /**
     * Adding logging feature to conversion method
     * @param currencyVariant
     * @param number
     * @return converted Currency
     */
    @Override
    public double umrechnen(String currencyVariant, double number) {
        Date datum = new java.sql.Date(System.currentTimeMillis());

        String text = "Convert to, " + currencyVariant + " with the value of: " + number;
        Log logging = new Log(text, datum);
        LogDAOImplementation daoLog = new LogDAOImplementation();
        System.out.println(text);
        daoLog.addLog(logging);

        return this.wrDecorator.umrechnen(currencyVariant, number);
    }
}
