package CurrencyConverter.CurrencyConverter.Decorator;

import CurrencyConverter.WR;

/**
 * Implementing the Decorator Pattern of the Currency Converter
 * @author Stefan Mikic
 * @version 1
 */
public abstract class WRDecorator extends WR {

    protected WR wrDecorator;

    /**
     * Constructor of the Decorator
     * @param wrDecorator
     */
    public WRDecorator(WR wrDecorator) {
        this.wrDecorator = wrDecorator;
    }

    /**
     * Overwriting getCurrencyVariant and return it with the wrDecorator.getCurrencyVariant
     *
     * @return Currency variant
     */
    @Override
    public String getCurrencyVariant() {
        return this.wrDecorator.getCurrencyVariant();
    }


    /**
     * Overwriting getRate and return it with the wrDecorator.getRate
     *
     * @return Rate
     */
    @Override
    public double getRate() {
        return this.wrDecorator.getRate();
    }


    @Override
    public abstract double umrechnen(String currencyVariant, double number);

}
