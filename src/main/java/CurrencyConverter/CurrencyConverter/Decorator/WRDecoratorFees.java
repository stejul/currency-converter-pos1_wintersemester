package CurrencyConverter.CurrencyConverter.Decorator;

import CurrencyConverter.WR;

/**
 * Currency converter Class which adds <b>fees</b> while inheriting from the WRDecorator class
 * @author Stefan Mikic
 * @version 1
 * @see WRDecorator
 */
public class WRDecoratorFees extends WRDecoratorLogging {
    private double decoratorFee;

    /**
     * Creating a Constructor with fees
     * @param wrDecorator
     * @param fee
     */
    public WRDecoratorFees(WR wrDecorator, double fee) {
        super(wrDecorator);
        this.decoratorFee = fee;
    }

    /**
     * Overwriting the method to add fees to the conversion
     * @param currencyVariant what type of currency should be converted
     * @param number Value
     * @return Converted value with reduced fees
     */
    @Override
    public double umrechnen(String currencyVariant, double number) {

        double reducedConversion = number-(number*(this.decoratorFee/100));

        return wrDecorator.umrechnen(currencyVariant, reducedConversion);

    }
}
