package CurrencyConverter;

/**
 * Interface for the conversion method
 * @author Stefan Mikic
 * @version 1
 */
public interface IUmrechnen {


    /**
     *
     * @param currencyVariant defines which currencies are being converted to
     * @param number value of the source currency
     * @return convertedNumber
     */
    double umrechnen(String currencyVariant, double number);
}
