package CurrencyConverter;

/**
 * Interface for bulk conversion
 * @author Stefan Mikic
 * @version 1
 */
public interface ISammelumrechnung {


    /**
     * Same as in IUmrechnen only for bulk conversion
     * @param currencyVariant
     * @param number
     * @return convertedNumber
     * @see IUmrechnen
     */
    double umrechnen(String currencyVariant, double[] number);
}
