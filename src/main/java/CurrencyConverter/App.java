package CurrencyConverter;

        import CurrencyConverter.CurrencyConverter.Decorator.WRDecorator;
        import CurrencyConverter.CurrencyConverter.Decorator.WRDecoratorLogging;
        import CurrencyConverter.CurrencyConverter.WRImplementation.Dollar2serbDinar;
        import CurrencyConverter.CurrencyConverter.WRImplementation.Euro2Dollar;
        import CurrencyConverter.CurrencyConverter.WRImplementation.Euro2Yen;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        System.out.println("Hello World!");

        WR eur = new Euro2Dollar();
        WR test1 = new Dollar2serbDinar();
        WRDecorator test = new WRDecoratorLogging(eur);

        test.umrechnen("EUR2USD", 5);

        eur.addNextSuccessor(test1);
        test1.addNextSuccessor(new Euro2Yen());

        System.out.println(eur.getSuccessor());
        eur.removeLastSuccessor();
    }
}
