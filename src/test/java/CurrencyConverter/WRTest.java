package CurrencyConverter;

import CurrencyConverter.CurrencyConverter.Decorator.WRDecoratorLogging;
import CurrencyConverter.CurrencyConverter.WRImplementation.Euro2Dollar;
import CurrencyConverter.CurrencyConverter.WRImplementation.Euro2Yen;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WRTest {

    private WR testWR;
    private WR testCalc;
    private WR anotherWR;

    @Before
    public void setup(){
        this.testWR = new Euro2Dollar();
        this.testCalc = new WRDecoratorLogging(testWR);
        this.anotherWR = new Euro2Yen();
    }

    @Test
   public void umrechnenTest(){

    double expectedValue = testCalc.umrechnen("EUR2USD", 2);
    double actualValue = testWR.umrechnen("EUR2USD",2);
    assertEquals(expectedValue, actualValue, 0);

    }



}