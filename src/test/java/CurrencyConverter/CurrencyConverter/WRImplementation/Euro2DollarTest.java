package CurrencyConverter.CurrencyConverter.WRImplementation;

import CurrencyConverter.WR;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Euro2DollarTest {

    private WR conv;

    @Before
    public void setUp() throws Exception {
        this.conv = new Euro2Dollar();
    }

    @Test
    public void umrechnenTest() {

        double expectedValue = conv.umrechnen("EUR2USD", 2);
        assertEquals(2.28, expectedValue, 0);
    }

    @Test
    public void addNextSuccessorTest(){
        WR testWR = new Euro2Yen();
        WR anotherTestWR = new Euro2Dollar();
        conv.addNextSuccessor(testWR);
        testWR.addNextSuccessor(anotherTestWR);
        assertEquals(testWR, conv.getSuccessor());
        assertEquals(anotherTestWR, testWR.getSuccessor());


    }

    @Test
    public void removeSuccessorTest(){
        assertNull(conv.getSuccessor());
        WR testWR = new Euro2Yen();
        conv.addNextSuccessor(testWR);
        assertEquals(testWR, conv.getSuccessor());
        conv.removeLastSuccessor();
        System.out.println(conv.getSuccessor());
        assertNull(conv.getSuccessor());
    }
}