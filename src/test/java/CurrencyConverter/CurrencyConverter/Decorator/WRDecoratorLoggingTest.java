package CurrencyConverter.CurrencyConverter.Decorator;

import CurrencyConverter.CurrencyConverter.WRImplementation.Euro2Yen;
import CurrencyConverter.WR;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WRDecoratorLoggingTest {

    private WR testWR;
    private WRDecoratorLogging loggingWR;
    @Before
    public void setUp() throws Exception {
        this.testWR = new Euro2Yen();
        this.loggingWR = new WRDecoratorLogging(testWR);

    }

    @Test
    public void umrechnenTest(){
        double expectedValue = testWR.umrechnen("EUR2YEN", 4);
        double actualValue = loggingWR.umrechnen("EUR2YEN", 4);
        assertEquals(expectedValue, actualValue, 0);
    }
}