package CurrencyConverter.CurrencyConverter.Decorator;

import CurrencyConverter.CurrencyConverter.WRImplementation.Euro2Yen;
import CurrencyConverter.WR;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WRDecoratorFeesTest {

    private WR testWR;
    private WRDecoratorFees testFees;

    @Before
    public void setUp() throws Exception {
        this.testWR = new Euro2Yen();
        this.testFees = new WRDecoratorFees(testWR, 3);

    }

    @Test
    public void umrechnenTest(){

        double expectedValue = testWR.umrechnen("EUR2YEN", 4.85); //Already used the fees on the number
        double actualValue = testFees.umrechnen("EUR2YEN", 5);
        assertEquals(expectedValue, actualValue, 0);
    }
}